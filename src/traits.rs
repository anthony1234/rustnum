pub trait Zero
{
    fn zero() -> Self;
}

pub trait One 
{
    fn one() -> Self;
}

pub trait Abs
{
    fn abs(&self) -> f64;
}