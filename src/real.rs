use crate::traits::{Zero, One, Abs};

impl Zero for f64 {
    fn zero() -> Self {
        0f64
    }
}

impl One for f64 {
    fn one() -> Self {
        1f64
    }
}

impl Abs for f64 {
    fn abs(&self) -> f64 
    {
        self.abs()
    }
}