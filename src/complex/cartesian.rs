use crate::complex::trigonometric::Trigonometric;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One, Abs};
use std::marker::{Sync, Send};

#[derive(Debug, Copy, Clone)]
pub struct Cartesian
{
    pub re: f64,
    pub im: f64
}

impl Cartesian 
{
    pub fn new(re_init: f64, im_init: f64) -> Cartesian 
    {
        let to_return = Cartesian {
            re: re_init,
            im: im_init
        };

        return to_return;
    }

    

    pub fn to_trigonometric(&self) -> Trigonometric 
    {
        Trigonometric {
            r: self.abs(),
            theta: (self.im/self.re).atan(),
        }
    }
}

impl Add for Cartesian {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            re: self.re + other.re,
            im: self.im + other.im,
        }
    }
}

impl Sub for Cartesian {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            re: self.re - other.re,
            im: self.im - other.im,
        }
    }
}

impl Neg for Cartesian {
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            re: -self.re,
            im: -self.im,
        }
    }
}

impl Mul for Cartesian {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            re: self.re*other.re -  self.im*other.im,
            im: self.im*other.re + other.im*self.re,
        }
    }
}

impl Div for Cartesian {
    type Output = Self;

    fn div(self, other: Self) -> Self {

        let aux: f64 = other.re.powi(2) + other.im.powi(2);

        Self {
            re: (self.re*other.re +  self.im*other.im)/aux,
            im: (self.im*other.re - other.im*self.re)/aux,
        }
    }
}

impl fmt::Display for Cartesian {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} + {}i", self.re, self.im)
    }
}

impl Zero for Cartesian {
    fn zero() -> Self {
        Self {
            re: 0f64,
            im : 0f64,
        }
    }
}

impl One for Cartesian {
    fn one() -> Self {
        Self {
            re: 1f64,
            im : 0f64,
        }
    }
}

unsafe impl Send for Cartesian {}
unsafe impl Sync for Cartesian {}

impl Abs for Cartesian 
{
    fn abs(&self) -> f64 
    {
        return (self.re.powi(2) + self.im.powi(2)).sqrt();
    }
}