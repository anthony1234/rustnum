use crate::complex::cartesian::Cartesian;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;

pub struct Trigonometric
{
    pub r: f64,
    pub theta: f64
}

impl Trigonometric 
{
    pub fn new(r_init: f64, theta_init: f64) -> Trigonometric 
    {
        let to_return = Trigonometric {
            r: r_init,
            theta: theta_init
        };

        return to_return;
    }

    pub fn abs(&self) -> f64 
    {
        return self.r;
    }

    pub fn to_cartesian(&self) -> Cartesian
    {
        Cartesian {
            re: self.r*(self.theta.cos()),
            im: self.r*(self.theta.sin()),
        }
    }
}

impl Add for Trigonometric {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            r: self.r + other.r,
            theta: self.theta + other.theta
        }
    }
}

impl Sub for Trigonometric {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            r: self.r - other.r,
            theta: self.theta - other.theta,
        }
    }
}

impl Neg for Trigonometric {
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            r: -self.r,
            theta: self.theta,
        }
    }
}

impl Mul for Trigonometric {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            r: self.r*other.r,
            theta: self.theta + other.theta,
        }
    }
}

impl Div for Trigonometric {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Self {
            r: self.r/other.r,
            theta: self.theta - other.theta,
        }
    }
}

impl fmt::Display for Trigonometric {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} * e^{}i", self.r, self.theta)
    }
}