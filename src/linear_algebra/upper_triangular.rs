/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Div};
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

// Upper Triangular matrices 
#[derive(Clone, Debug)]
pub struct UpperTriangular<T>
{
    // Size's Matrix
    pub n: usize,

    // Values
    pub v: Vec<T>
}

impl<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send> UpperTriangular<T>
{
    pub fn new(n_init: usize, x: T) -> UpperTriangular<T>
    {
        let to_return = UpperTriangular {
            n: n_init,
            // We only need to save over diagonal elements
            v: vec![x;((n_init)*(n_init+1))/2]
        };

        return to_return;
    }

    pub fn zeros(n_init: usize) -> UpperTriangular<T>
    {
        let to_return = UpperTriangular {
            n: n_init,
            // We only need to save over diagonal elements
            v: vec![T::zero();((n_init)*(n_init+1))/2]
        };

        return to_return;
    }

    pub fn print(&self)
    {
        let mut self_ij: usize;
	
        for i in 0..self.n
	    {
            print!("[");

            for _ in 0..i
            {
                print!(" {} ", 0.0);
            }
            
            for j in i..self.n
            {
                print!(" {} ", self.fast_get(i,j));
            }

            println!("]");
	    }
    }

    pub fn set(&mut self, i: usize, j: usize, x: T)
    {
        if j < self.n && i <= j
        {

            let self_ij = ((i as isize)*((2*self.n as isize) - (i as isize) + 1)/(2 as isize) + (j as isize) - (i as isize)) as usize;

            self.v[self_ij] = x;
        }
        else
        {
            panic!("rustnum error : attempt to access to non upper element in upper triangular matrix.");
        }
    }

    pub fn fast_set(&mut self, i: usize, j: usize, x: T)
    {
        let self_ij = ((i as isize)*((2*self.n as isize) - (i as isize) + 1)/(2 as isize) + (j as isize) - (i as isize)) as usize;

        self.v[self_ij] = x;
    }

    pub fn get(&self, i: usize, j: usize) -> T
    {
        if j < self.n && i <= j
        {

            let self_ij = ((i as isize)*((2*self.n as isize) - (i as isize) + 1)/(2 as isize) + (j as isize) - (i as isize)) as usize;

            return self.v[self_ij];
        }
        else
        {
            panic!("rustnum error : attempt to access to non upper element in upper triangular matrix.");
        }
    }

    pub fn fast_get(&self, i: usize, j: usize) -> T
    {

        let self_ij = ((i as isize)*((2*self.n as isize) - (i as isize) + 1)/(2 as isize) + (j as isize) - (i as isize)) as usize;

        return self.v[self_ij];
    }

    /*
    pub fn block_decomposition(&self, given_from: usize, given_to: usize) -> UpperTriangular
    {
        /*
        Extract the block between given indexes
         */

        let to_return = UpperTriangular {
            n: given_to - given_from,
            sub_matrix: true,
            from: given_from + self.from,
            v: Rc::clone(&self.v)
        };

        return to_return;
    }

    pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
    }
    */
}
