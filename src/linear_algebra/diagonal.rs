/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Div};
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

// Diagonals matrices
pub struct Diagonal<T>
{
    // Size's Matrix
    pub n: usize,

    // Values
    pub v: Vec<T>
}

impl<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send> Diagonal<T>
{
    pub fn new(n_init: usize, x: T) -> Diagonal<T>
    {
        let to_return = Diagonal {
            n: n_init,
	        // We only need to save diagonal values 
            v: vec![x;n_init]
        };

        return to_return;
    }

    pub fn zeros(n_init: usize) -> Diagonal<T>
    {
        let to_return = Diagonal {
            n: n_init,
	        // We only need to save diagonal values 
            v: vec![T::zero();n_init]
        };

        return to_return;
    }

    pub fn eye(n_init: usize) -> Diagonal<T>
    {
        let to_return = Diagonal {
            n: n_init,
	        // We only need to save diagonal values 
            v: vec![T::one();n_init]
        };

        return to_return;
    }
    
    pub fn print(&self)
    {
        for i in 0..self.n
        {
            print!("[");

            for j in 0..self.n
            {

                if i == j
                {
                    print!(" {} ", self.v[i]);
                }
                else
                {
                    print!(" {} ", 0.0);
                }
            }

            println!("]")
        }
    }

    pub fn set(&mut self, i: usize, x: T)
    {
        if i < self.n
        {
            self.v[i] = x;
        }
    }

    pub fn fast_set(&mut self, i: usize, x: T)
    {
        self.v[i] = x;
    }

    pub fn get(&self, i: usize) -> T 
    {
        if i < self.n
        {
            return self.v[i];
        }
        else
        {
            panic!("Attempt to access to out of bound element in diagonal matrix.")
        }
    }

    pub fn fast_get(&self, i: usize) -> T 
    {
        return self.v[i];
    }

    /*
    pub fn block_decomposition(&self, given_from: usize, given_to: usize) -> Diagonal
    {
        /*
        Extract the block between given indexes
        */

        let to_return = Diagonal {
            n: given_to - given_from,
            sub_matrix: true,
            values_n: self.n,
            from: given_from + self.from,
            to: given_to,
            v: Rc::clone(&self.v)
        };

        return to_return;
    }

    pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
    }
    */

}
