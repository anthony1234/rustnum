/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Div};
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

// Lower Triangular matrices 
#[derive(Clone, Debug)]
pub struct LowerTriangular<T>
{
    // Size's Matrix
    pub n: usize,

    // Values
    pub v: Vec<T>
}

impl<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send> LowerTriangular<T>
{
    pub fn new(n_init: usize, x: T) -> LowerTriangular<T>
    {
        let to_return = LowerTriangular {
            n: n_init,
            // We only save under diagonal elements
            v: vec![x;((n_init)*(n_init+1))/2]
        };

        return to_return;
    }

    pub fn zeros(n_init: usize) -> LowerTriangular<T>
    {
        let to_return = LowerTriangular {
            n: n_init,
            // We only save under diagonal elements
            v: vec![T::zero();((n_init)*(n_init+1))/2]
        };

        return to_return;
    }

    pub fn eye(n_init: usize) -> LowerTriangular<T>
    {
        let mut to_return = LowerTriangular {
            n: n_init,
            // We only save under diagonal elements
            v: vec![T::zero();((n_init)*(n_init+1))/2]
        };

        for i in 0..n_init 
        {
            to_return.fast_set(i,i, T::one());
        }

        return to_return;
    }
    

    pub fn print(&self)
    {
        for i in 0..self.n
	    {
            print!("[");
            
            for j in 0..(i+1)
            {
                print!(" {} ", self.fast_get(i,j));
            }

            for _ in (i+1)..self.n
            {
                print!(" {} ", 0.0);
            }

            println!("]");
	    }
    }

    pub fn set(&mut self, i: usize, j: usize, x: T)
    {
        if i < self.n && j <= i
        {
            // This is the formula which allow us to access to elements
            let self_ij = ((((i as isize)*((i as isize)-1))/2) as usize) + i+j;

            self.v[self_ij] = x;
        }
        else
        {
            panic!("rustnum error : attempt to access to non upper element in upper triangular matrix.");
        }
    }

    pub fn fast_set(&mut self, i: usize, j: usize, x: T)
    {
        // Same that set but without condition block 
        
        let self_ij = ((((i as isize)*((i as isize)-1))/2) as usize) + i+j;

        self.v[self_ij] = x;
    }

    pub fn get(&self, i: usize, j: usize) -> T 
    {
        if i < self.n && j <= i
        {
            let self_ij = ((((i as isize)*((i as isize)-1))/2) as usize) + i+j;

            return self.v[self_ij];
        }
        else
        {
            panic!("rustnum error : attempt to access to non upper element in upper triangular matrix.");
        }
    }

    pub fn fast_get(&self, i: usize, j: usize) -> T 
    {
        // Same that get but without condition block 

        let self_ij = ((((i as isize)*((i as isize)-1))/2) as usize) + i+j;

        return self.v[self_ij];
    }

    /*
    pub fn block_decomposition(&self, given_from: usize, given_to: usize) -> LowerTriangular
    {
        /*
        Extract the block between given indexes
         */

        let to_return = LowerTriangular {
            n: given_to - given_from,
            sub_matrix: true,
            from: given_from + self.from,
            v: Rc::clone(&self.v)
        };

        return to_return;
    }

    pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
    }
    */

}
