/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

pub mod operations;

pub mod dense;
pub mod diagonal;
pub mod tridiagonal;
pub mod upper_triangular;
pub mod symmetric;
pub mod lower_triangular;
//pub mod sparse;

/*
Matrices à implémenter :
- Dense2s  v
- Diagonales  v
- Triangulaires v
- Symétriques v
- Sparses
- Tridiagonales v
- Bandes
 */
