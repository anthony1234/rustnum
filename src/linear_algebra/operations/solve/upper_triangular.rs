/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense; 
use crate::linear_algebra::upper_triangular::UpperTriangular;
use crate::linear_algebra::operations::basics::*;
use rayon::prelude::*;
use std::sync::{Mutex, Arc};
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One, Abs};
use std::marker::{Sync, Send};

pub fn default<T>(a: &UpperTriangular<T>, b: &Dense<T>, x: &mut Dense<T>)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Solve linear system 
    a x = b 

    b and x are column matrix 

    Perform ascent algorithm
    */

    let n = a.n; 

    if b.n == n && b.m == 1 && x.n == n && x.m == 1
    {
        x.fast_set(n-1,0, b.fast_get(n-1,0)/a.fast_get(n-1,n-1));

        let mut sum: T = T::zero();

        for i in (0..n).rev() 
        {
            sum = T::zero();

            for k in (i+1)..n 
            {
                sum = sum + a.fast_get(i,k)*x.fast_get(k,0);
            }

            x.set(i,0, (T::one()/a.fast_get(i,i))*(b.fast_get(i,0)-sum));
        }
    }
    else 
    {
        panic!("Impossible to solve system : inconscistent matrix sizes.")
    }
}