/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense; 
use crate::linear_algebra::lower_triangular::LowerTriangular;
use crate::linear_algebra::upper_triangular::UpperTriangular;
use crate::linear_algebra::operations::basics::*;
use rayon::prelude::*;
use std::sync::{Mutex, Arc};
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One, Abs};
use std::marker::{Sync, Send};

// fortement fausses, à revoir
pub fn gauss_seidel<T>(a: &Dense<T>, b: &Dense<T>, x: &mut Dense<T>, tol: f64)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Solve the linear system 
    ax = b

    b and x are column vectors
    a is a matrix 

    Initialy x is set by user.

    tol is a tolerance to break while
    */ 

    // A TESTER 
    if a.n == a.m && b.n == a.n && b.m == 1 && x.n == a.n && x.m == 1
    {
        let n = a.n;
        let mut sum: T = T::zero();
        let mut error: f64 = tol + 1.0;

        let mut b_estimated = Dense::<T>::zeros(n,1);
        let mut residual = Dense::<T>::zeros(n,1);

        while error > tol 
        {
            for i in 0..n
            {
                sum = T::zero();

                for j in 0..n
                {
                    // Sum calculus
                    if i != j
                    {
                        sum = sum + a.fast_get(i,j)*x.fast_get(j,0);
                    }
                }

                sum = (-T::one()/a.fast_get(i,i))*sum + b.fast_get(i,0)/a.fast_get(i,i);

                // Save value 
                x.fast_set(i,0,sum);
            }

            // Error evaluation 
            product::dense_dense(&a, &x, &mut b_estimated);

            linear_combinaison::dense_dense(T::one(), &b, -T::one(), &b_estimated, &mut residual);

            error = norm::dense(&residual);
        }
    }
}

pub fn jacobi<T>(a: &Dense<T>, b: &Dense<T>, x: &mut Dense<T>, tol: f64)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Solve the linear system 
    ax = b

    b and x are column vectors
    a is a matrix 

    Initialy x is fast_set by user.

    tol is a tolerance to break while
    */ 

    if a.n == a.m && b.n == a.n && b.m == 1 && x.n == a.n && x.m == 1
    {
        let n = a.n;
        let mut sum: T = T::zero();
        let mut error: f64 = tol + 1.0;

        let mut x_prec: Vec<T> = x.v.clone();

        while error > tol 
        {
            for i in 0..n
            {
                sum = T::zero();

                for j in 0..n
                {
                    // Sum calculus
                    if i != j
                    {
                        sum = sum + a.fast_get(i,j)*x_prec[j];
                    }
                }

                sum = (T::one()/a.fast_get(i,i))*(-sum + b.fast_get(i,0));

                // Save value 
                x.fast_set(i,0,sum);
            }

            error = 0f64;

            // x actualisation and error calculus
            for i in 0..n 
            {
                error = error + (x_prec[i] - x.fast_get(i,0)).abs().powi(2);

                x_prec[i] = x.fast_get(i,0);
            }
        }
    }
}

pub fn par_jacobi<T>(a: &Dense<T>, b: &Dense<T>, x: &mut Dense<T>, tol: f64)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Solve the linear system 
    ax = b

    b and x are column vectors
    a is a matrix 

    Initialy x is fast_set by user.

    tol is a tolerance to break while
    */ 

    // A TESTER 
    if a.n == a.m && b.n == a.n && b.m == 1 && x.n == a.n && x.m == 1
    {
        let n = a.n;
        let mut error: f64 = tol + 1.0;

        let mut x_prec = x.v.clone();
        let x_thread = Arc::new(Mutex::new(&mut x.v));

        let iterator: Vec<usize> = (0..n).collect();

        while error > tol 
        {
            println!("{}", error);
            iterator.par_iter().for_each(|i|
            {
                let mut sum: T = T::zero();

                for j in 0..n
                {
                    // Sum calculus
                    if *i != j
                    {
                        sum = sum + a.v[*i+j*n]*x_prec[j];
                    }
                }

                sum = (T::one()/a.v[*i+(*i)*n])*(-sum + b.v[*i]);

                // Save value 
                x_thread.lock().unwrap()[*i] = sum;
            });

            error = 0f64;

            // x actualisation and error calculus
            for i in 0..n 
            {
                //error = error + (x_prec[i] - x.fast_get(i,0)).powi(2);

                error = error + (x_prec[i] - x_thread.lock().unwrap()[i]).abs().powi(2);

                //x_prec[i] = x.fast_get(i,0);

                x_prec[i] = x_thread.lock().unwrap()[i];
            }

            error = error.sqrt();
        }
    }
}