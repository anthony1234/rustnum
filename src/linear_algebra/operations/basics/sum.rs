/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense;
use crate::linear_algebra::diagonal::Diagonal;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

pub fn dense_dense<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send>(x: T, a: &Dense<T>, y: T, b: &Dense<T>, s: &mut Dense<T>)
{
    /*
    Compute s <- a+b
    */

    if a.n == b.n && a.m == b.m && s.n == a.n && s.m == a.m
    {
        for i in 0..(a.n*a.m)
        {
            s.v[i] = a.v[i] + b.v[i];
        }
        
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}

pub fn diagonal_diagonal<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send>(x: T, a: &Diagonal<T>, y: T, b: &Diagonal<T>, s: &mut Diagonal<T>)
{
    if a.n == b.n && a.n == s.n 
    {
        for i in 0..(a.n)
        {
            s.v[i] = a.v[i] + b.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}

pub fn dense_diagonal<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send>(x: T, a: &Dense<T>, y: T, b: &Diagonal<T>, s: &mut Dense<T>)
{

    if a.n == b.n && a.m == b.n && s.n == a.n && s.m == a.m
    {
        for k in 0..a.n
        {
            s.v[k*(s.n+1)] = a.v[k*(a.n+1)] + b.v[k]; 
        }
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}

pub fn diagonal_dense<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send>(x: T, b: &Diagonal<T>, y: T, a: &Dense<T>, s: &mut Dense<T>)
{
    if a.n == b.n && a.m == b.n && s.n == a.n && s.m == a.m
    {
        for k in 0..a.n
        {
            s.v[k*(s.n+1)] = a.v[k*(a.n+1)] + b.v[k]; 
        }
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}
