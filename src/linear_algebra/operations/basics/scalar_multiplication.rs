/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense;
use crate::linear_algebra::diagonal::Diagonal;
use crate::linear_algebra::lower_triangular::LowerTriangular;
use crate::linear_algebra::symmetric::Symmetric;
use crate::linear_algebra::tridiagonal::Tridiagonal;
use crate::linear_algebra::upper_triangular::UpperTriangular;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

pub fn dense<T>(x: T, a: &Dense<T>, p: &mut Dense<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n && a.m == p.m 
    {
        for i in 0..(a.n*a.m)
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}

pub fn diagonal<T>(x: T, a: &Diagonal<T>, p: &mut Diagonal<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n
    {
        for i in 0..a.n
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}

pub fn lower_triangular<T>(x: T, a: &LowerTriangular<T>, p: &mut LowerTriangular<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n
    {
        for i in 0..(((a.n)*(a.n+1))/2)
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}

pub fn symmetric<T>(x: T, a: &Symmetric<T>, p: &mut Symmetric<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n
    {
        for i in 0..(((a.n)*(a.n+1))/2)
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}

pub fn tridiagonal<T>(x: T, a: &Tridiagonal<T>, p: &mut Tridiagonal<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n
    {
        for i in 0..(((a.n)*(a.n+1))/2)
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}

pub fn upper_triangular<T>(x: T, a: &UpperTriangular<T>, p: &mut UpperTriangular<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == p.n
    {
        for i in 0..(3*(a.n-2)+4)
        {
            p.v[i] = x*a.v[i];
        }
    }
    else
    {
        panic!("Inconsistent matrix size in scalar multiplication.");
    }
}