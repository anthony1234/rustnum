/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense; 
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One, Abs};
use std::marker::{Sync, Send};

pub fn dense<T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send>(v: &Dense<T>) -> f64
{
    // Compute euclidian norm of v 

    let mut sum: f64 = 0f64;

    if v.n == 1 
    {
        let m = v.m;
        let mut v_i: T;

        for i in 0..m
        {
            v_i = v.fast_get(0,i);

            sum = sum + v_i.abs().powi(2);
        }

        return sum.sqrt();
    }
    else if v.m == 1
    {
        let n = v.n;
        let mut v_i: T;

        for i in 0..n
        {
            v_i = v.fast_get(i,0);

            sum = sum + v_i.abs().powi(2);
        }

        return sum.sqrt();
    }
    else 
    {
        panic!("Matrix norm no build yet.");
    }
}