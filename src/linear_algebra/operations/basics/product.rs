/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense;
use crate::linear_algebra::diagonal::Diagonal;
use crate::linear_algebra::lower_triangular::LowerTriangular;
use crate::linear_algebra::upper_triangular::UpperTriangular;
use crate::linear_algebra::operations::basics::*;
use rayon::prelude::*;
use std::sync::{Mutex, Arc};
use std::cmp;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

pub fn dense_dense<T>(a: &Dense<T>, b: &Dense<T>, p: &mut Dense<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    let mut inter: T;

    if a.m == b.n && p.n == a.n && p.m == b.m
    {
        for i in 0..p.n
        {
            for j in 0..p.m
            {
                inter = T::zero();

                for k in 0..a.m
                {
                    inter = inter + a.fast_get(i,k)*b.fast_get(k,j);
                }

                p.fast_set(i,j,inter);
            }
        }
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}

pub fn par_dense_dense<T>(a: &Dense<T>, b: &Dense<T>, p: &mut Dense<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    // A améliorer 
    //let a_thread = Arc::new(&a.v);
    //let b_thread = Arc::new(&b.v);
    

    if a.m == b.n && p.n == a.n && p.m == b.m
    {
        let n = p.n;
        let m = p.m;

        let mut p_thread = Arc::new(Mutex::new(p));

        let iterator: Vec<usize> = (0..n).collect();

        iterator.par_iter().for_each(|i|
        {
            let mut inter: T;

            for j in 0..m
            {
                inter = T::zero();

                for k in 0..a.m
                {
                    inter = inter + a.fast_get(*i,k)*b.fast_get(k,j);
                }

                (p_thread.lock().unwrap()).fast_set(*i,j, inter);
            }
        })
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}

/*
pub fn strassen_dense_dense(a: &Dense<T>, b: &Dense<T>, p: &mut Dense<T>)
{
    if a.m == b.n && p.n == a.n && p.m == b.m
    {
        if (a.n as T).ln() > 7T
        {
            // Strassen Algorithm

            // Decomposition
            let a_n_two = a.n/2usize;

            let a_1_1 = a.block_decomposition(0, a_n_two, 0, a_n_two);
            let a_2_1 = a.block_decomposition(a_n_two, a.n, 0, a_n_two);
            let a_1_2 = a.block_decomposition(0, a_n_two, a_n_two, a.n);
            let a_2_2 = a.block_decomposition(a_n_two, a.n, a_n_two, a.n);

            let b_1_1 = b.block_decomposition(0, a_n_two, 0, a_n_two);
            let b_2_1 = b.block_decomposition(a_n_two, a.n, 0, a_n_two);
            let b_1_2 = b.block_decomposition(0, a_n_two, a_n_two, a.n);
            let b_2_2 = b.block_decomposition(a_n_two, a.n, a_n_two, a.n);

            let mut p_1_1 = p.block_decomposition(0, a_n_two, 0, a_n_two);
            let mut p_2_1 = p.block_decomposition(a_n_two, a.n, 0, a_n_two);
            let mut p_1_2 = p.block_decomposition(0, a_n_two, a_n_two, a.n);
            let mut p_2_2 = p.block_decomposition(a_n_two, a.n, a_n_two, a.n);

            let mut inter1 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut inter2 = Dense<T>::zeros(a_n_two, a_n_two);

            let mut m_1 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_2 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_3 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_4 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_5 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_6 = Dense<T>::zeros(a_n_two, a_n_two);
            let mut m_7 = Dense<T>::zeros(a_n_two, a_n_two);

            // M_1 calculus
            sum::dense_dense(&a_1_1, &a_2_2, &mut inter1);
            sum::dense_dense(&b_1_1, &b_2_2, &mut inter2);
            strassen_dense_dense(&inter1, &inter2, &mut m_1);

            // M_2 calculus
            sum::dense_dense(&a_2_1, &a_2_2, &mut inter1);
            strassen_dense_dense(&inter1, &b_1_1, &mut m_2);

            // M_3 calculus
            linear_combinaison::dense_dense(1T, &b_1_2, -1T, &b_2_2, &mut inter1);
            strassen_dense_dense(&a_1_1, &inter1, &mut m_3);

            // M_4 calculus
            linear_combinaison::dense_dense(1T, &b_2_1, -1T, &b_1_1, &mut inter1);
            strassen_dense_dense(&a_2_2, &inter1, &mut m_4);

            // M_5 calculus
            sum::dense_dense(&a_1_1, &a_1_2, &mut inter1);
            strassen_dense_dense(&inter1, &b_2_2, &mut m_5);

            // M_6 calculus
            linear_combinaison::dense_dense(1T, &a_2_1, -1T, &a_1_1, &mut inter1);
            sum::dense_dense(&b_1_1, &b_1_2, &mut inter2);
            strassen_dense_dense(&inter1, &inter2, &mut m_6);

            // M_7 calculus
            linear_combinaison::dense_dense(1T, &a_1_2, -1T, &a_2_2, &mut inter1);
            sum::dense_dense(&b_2_1, &b_2_2, &mut inter2);
            strassen_dense_dense(&inter1, &inter2, &mut m_7);

            // Result
            sum::dense_dense(&m_1, &m_4, &mut inter1);
            linear_combinaison::dense_dense(-1T, &m_5, 1T, &m_7, &mut inter2);;
            sum::dense_dense(&inter1, &inter2, &mut p_1_1);

            sum::dense_dense(&m_3, &m_5, &mut p_1_2);

            sum::dense_dense(&m_2, &m_4, &mut p_2_1);

            sum::dense_dense(&m_3, &m_6, &mut inter1);
            linear_combinaison::dense_dense(-1T, &m_2, 1T, &m_1, &mut inter2);
            sum::dense_dense(&inter1, &inter2, &mut p_2_2);
        }
        else
        {
            dense_dense(a, b, p);;
        }
    }
    else
    {
        panic!("Inconsistent matrix size in sum");
    }
}
*/

pub fn diagonal_dense<T>(a: &Diagonal<T>, b: &Dense<T>, p: &mut Dense<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    if a.n == b.n && p.n == a.n && p.m == b.m
    {
        for i in 0..p.n
        {
            for j in 0..p.m
            {
                p.fast_set(i,j,a.fast_get(i)*b.fast_get(i,j));
            }
        }
    }
    else
    {
        panic!("Inconsistent matrix size in product");
    }        
}

pub fn lower_triangular_upper_triangular<T>(a: &LowerTriangular<T>, b: &UpperTriangular<T>, p: &mut Dense<T>)
where T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    let n = a.n;

    if p.n == n && p.m == n && b.n == n
    {
        let mut sum = T::zero();
        let mut max_sum: usize;

        for i in 0..n
        {
            for j in 0..n
            {
                max_sum = cmp::min(i,j)+1;
                sum = T::zero(); 

                for k in 0..max_sum
                {
                    sum = sum + a.fast_get(i,k)*b.fast_get(k,j);
                }

                p.fast_set(i,j,sum);
            }
        }
    }
    else
    {
        panic!("Inconsistent matrix size in product");
    }  
}