/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::linear_algebra::dense::Dense; 
use crate::linear_algebra::lower_triangular::LowerTriangular;
use crate::linear_algebra::upper_triangular::UpperTriangular;
use rayon::prelude::*;
use std::sync::{Mutex, Arc, RwLock};
use std::borrow::Borrow;
use std::ops::{Add, Sub, Mul, Neg, Div};
use std::fmt;
use crate::traits::{Zero, One, Abs};
use std::marker::{Sync, Send};

pub fn dense<T>(a: &Dense<T>) -> (LowerTriangular<T>, UpperTriangular<T>)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Implement LU decomposition for dense matrices
    This function use Doolitle Algorithm 
    */ 

    if a.n == a.m 
    {
        let n = a.n;

        let mut l = LowerTriangular::<T>::eye(n);
        let mut u = UpperTriangular::<T>::zeros(n); 

        let mut sum: T = T::zero();

        for i in 0..(n-1)
        {
            for j in i..n 
            {
                // Sum calculus 
                sum = T::zero();

                for k in 0..i
                {
                    sum = sum + l.fast_get(i,k)*u.fast_get(k,j);
                }


                u.fast_set(i,j, a.fast_get(i,j) - sum);
            }

            for j in (i+1)..n 
            {
                // Sul calculus
                sum = T::zero();

                for k in 0..i
                {
                    sum = sum + l.fast_get(j,k)*u.fast_get(k,i);
                }

                l.fast_set(j,i, (T::one()/u.fast_get(i,i)*(a.fast_get(j,i)-sum)));
            }
        }

        // Sum calculus 
        sum = T::zero(); 

        for k in 0..n
        {
            sum = sum + l.fast_get(n-1,k)*u.fast_get(k,n-1);
        }

        u.fast_set(n-1, n-1, a.fast_get(n-1,n-1) - sum);

        return (l,u);
    }
    else
    {
        panic!("Impossible LU factorization");
    }
}

pub fn par_dense<T>(a: &Dense<T>) -> (LowerTriangular<T>, UpperTriangular<T>)
where T: Abs + Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Neg<Output = T> + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send
{
    /*
    Implement LU decomposition for dense matrices
    This function use Doolitle Algorithm 
    */ 
    if a.n == a.m 
    {
        let n = a.n;

        let mut l = LowerTriangular::<T>::eye(n);
        let mut u = UpperTriangular::<T>::zeros(n); 

        // Variables for parallel operations 
        let mut iterator: Vec<usize>;
        let mut v = Arc::new(Mutex::new(vec![T::zero();n]));


        for i in 0..(n-1)
        {
            iterator = (i..n).collect();

            iterator.par_iter().for_each(|j|
            {
                // Sum calculus 
                let mut sum = T::zero();

                for k in 0..i
                {
                    sum = sum + l.fast_get(i,k)*u.fast_get(k,*j);
                }


                v.lock().unwrap()[*j] = a.fast_get(i,*j) - sum;
            });

            for j in i..n 
            {
                u.fast_set(i,j, v.lock().unwrap()[j]);
            }

            iterator = ((i+1)..n).collect();

            iterator.par_iter().for_each(|j|
            {
                // Sul calculus
                let mut sum = T::zero();

                for k in 0..i
                {
                    sum = sum + l.fast_get(*j,k)*u.fast_get(k,i);
                }

                v.lock().unwrap()[*j] =  (T::one()/u.fast_get(i,i))*(a.fast_get(*j,i)-sum);
            });

            for j in (i+1)..n 
            {
                l.fast_set(j,i, v.lock().unwrap()[j]);
            }
        }

        // Sum calculus 
        let mut sum = T::zero(); 

        for k in 0..n
        {
            sum = sum + l.fast_get(n-1,k)*u.fast_get(k,n-1);
        }

        u.fast_set(n-1, n-1, a.fast_get(n-1,n-1) - sum);

        return (l,u);
    }
    else
    {
        panic!("Impossible LU factorization");
    }
}