/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

// Not works

use std::rc::Rc;
use std::cell::RefCell;

// Sparse matrices 
#[derive(Clone)]
pub struct Sparse
{
    // Size's Matrix
    pub n: usize,
    pub m: usize,

    // This matrix is a sub matrix of another or not
    pub sub_matrix: bool,

    // This value is necessary to access to values
    pub values_n: usize,

    // Indexes borrowing the matrix
    pub from_rows: usize,

    pub from_columns: usize,

    // Values
    pub v: Rc<RefCell<Vec<T>>>

    // Row indexes
    pub r: Rc<RefCell<Vec<T>>>

    // Column cumsum indexes 
    pub c: Rc<RefCell<Vec<T>>>
}

impl Sparse
{
    pub fn zeros(n_init: usize, m_init: usize) -> Sparse
    {
        let to_return = Sparse {
            n: n_init,
            m: m_init,
            sub_matrix: false,
            values_n: n_init,
            from_rows: 0usize,
            from_columns: 0usize,
            v: Rc::new(RefCell::new(Vec::new())),
            r: Rc::new(RefCell::new(Vec::new())),
            c: Rc::new(RefCell::new(vec![0T]))
        };

        return to_return;
    }

    pub fn print(&self)
    {
        let mut self_ij: usize;

        for i in 0..self.n
        {
            print!("[");

            for j in 0..self.m
            {
                self_ij = i + self.from_rows + (j + self.from_columns)*self.values_n;

                print!(" {} ", (self.v.borrow())[self_ij]);
            }

            println!("]")
        }
    }

    pub fn set(&mut self, i: usize, j: usize, x: T)
    {
        if i < self.n && j < self.m
        {
            let mut self.v = self.v.borrow_mut();

            let self_ij = i + self.from_rows + (j + self.from_columns)*self.values_n;

            self.v[self_ij] = x;
        }
	else
	{
	    panic!("rustnum error : attempt to access to out of bound element in sparse matrix.");
	}
    }

    pub fn block_decomposition(&self, given_from_rows: usize, given_to_rows: usize,
                               given_from_columns: usize, given_to_columns: usize) -> Sparse
    {
        /*
        Extract the block between given indexes
         */

        let to_return = Sparse {
            n: given_to_rows - given_from_rows,
            m: given_to_columns - given_from_columns,
            sub_matrix: true,
            values_n: self.n,
            from_rows: given_from_rows + self.from_rows,
            from_columns: given_from_columns + self.from_columns,
            to_rows: given_to_rows,
            to_columns: given_to_columns,
            v: Rc::clone(&self.v)
        };

        return to_return;
    }

    pub fn new(a: Dense)
    {
        // Create sparse matrix from dense matrix
    }

    pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
	}
}
