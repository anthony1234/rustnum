/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

/* !!!!! DON'T WORK !!!!!*/

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Div};
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

// Tridiagonals matrices
pub struct Tridiagonal<T>
{
    // Size's Matrix
    pub n: usize,

    // Values
    pub v: Vec<T>
}

impl<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send> Tridiagonal<T>
{
    pub fn new(n_init: usize, x: T) -> Tridiagonal<T>
    {
        let to_return = Tridiagonal {
            n: n_init,
	    	// We only need to save diagonal, subdiagonal an surdiagonal values 
            v: vec![x;3*(n_init-2)+4]
        };

        return to_return;
	}
	
	pub fn zeros(n_init: usize) -> Tridiagonal<T>
    {
        let to_return = Tridiagonal {
            n: n_init,
	    	// We only need to save diagonal, subdiagonal an surdiagonal values 
            v: vec![T::zero();3*(n_init-2)+4]
        };

        return to_return;
	}
	
	/*
    pub fn ones(n_init: usize) -> Tridiagonal<T>
    {
        let to_return = Tridiagonal {
            n: n_init,
            v: vec![1T;3*(n_init-2)+4]
        };

        return to_return;
	}
	*/
    
    pub fn print(&self)
    {
        let mut self_ii: usize = 0;

		// First line
		print!("[");

		print!(" {} ", self.v[0]);

		print!(" {} ", self.v[2]);

		for _ in 0..(self.n-2)
		{
			print!(" {} ", 0.0);
		}

		println!("]");
		
			for i in 2..(self.n)
			{
				// i th line 
				print!("[");

				// There is i-1 zeros at left of tridiagonal
				for _ in 0..(i-2)
				{
					print!(" {} ", 0.0);
				}

				print!(" {} ", self.v[3*(i-2)+1]);
				
				// Diagonal element 
				print!(" {} ", self.v[3*(i-1)]);

				print!(" {} ", self.v[3*(i-1)+2]);
				
				for _ in 0..(self.n-1-i)
				{
					print!(" {} ", 0.0);
				}

				println!("]")
			}

		// Last line 
		print!("[");

		for _ in 0..(self.n-2)
		{
			print!(" {} ", 0.0);
		}
		
		print!(" {} ", self.v[3*(self.n-2)+1]);

		print!(" {} ", self.v[3*(self.n-1)]);

		println!("]");
    }

    pub fn set(&mut self, i: usize, j: usize, x: T)
    {
		let i_j: isize = (i as isize) - (j as isize);

		if i < self.n && j < self.n
		{
			if i_j == 0
			{
				// We set diagonal element
				self.v[3*i] = x;
			}
			else if i_j == 1
			{
				// We set subdiagonal element
				self.v[3*(i-1)+1] = x;
			}
			else if i_j == -1
			{
				// We set surdiagonal element 
				self.v[3*(i+1)-1] = x;
			}
			else
			{
				panic!("rustnum error : attempt to access to non tridiagonal value in tridiagonal matrix.");
			}
		}
		else
		{
			panic!("rustnum error : attempt to access to out of bound value in tridiagonal matrix.");
		}
    }

	/*
    pub fn block_decomposition(&self, given_from: usize, given_to: usize) -> Tridiagonal
    {
        /*
        Extract the block between given indexes
         */

        let to_return = Tridiagonal {
            n: given_to - given_from,
            sub_matrix: true,
            from: given_from + self.from,
            v: Rc::clone(&self.v)
        };

        return to_return;
	}
	
	pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
	}
	*/
}
