/*
Copyright 2020 Anthony Gerber-Roth
This file is part of rustnum.

rustnum is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rustnum is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rustnum.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Div};
use crate::traits::{Zero, One};
use std::marker::{Sync, Send};

// Standard matrices 
#[derive(Clone)]
pub struct Dense<T>
{
    // Size's Matrix
    pub n: usize,
    pub m: usize,

    // Values
    pub v: Vec<T>
}
// <T: impl Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone>
impl<T: Add + Sub + Neg + Mul + Div + fmt::Display + Copy + Clone + Zero + One + Add<Output = T> + Sub<Output = T> + Mul<Output = T> + Div<Output = T> + Sync + Send> Dense<T>
{
    pub fn new(n_init: usize, m_init: usize, x: T) -> Dense<T>
    {
        let to_return = Dense {
            n: n_init,
            m: m_init,
            v: vec![x;n_init*m_init]
        };

        return to_return;
    }

    pub fn zeros(n_init: usize, m_init: usize) -> Dense<T>
    {
        let to_return = Dense {
            n: n_init,
            m: m_init,
            v: vec![T::zero();n_init*m_init]
        };

        return to_return;
    }

    pub fn print(&self)
    {
        let mut self_ij: usize;

        for i in 0..self.n
        {
            print!("[");

            for j in 0..self.m
            {
                print!(" {} ", self.fast_get(i,j));
            }

            println!("]");
        }
    }

    pub fn set(&mut self, i: usize, j: usize, x: T)
    {
        if i < self.n && j < self.m
        {
            self.v[j + i*self.m] = x;
        }
        else
        {
            panic!("rustnum error : attempt to access to out of bound element in Dense matrix.");
        }
    }

    pub fn fast_set(&mut self, i: usize, j: usize, x: T)
    {
        self.v[j + i*self.m] = x;
    }

    pub fn get(& self, i: usize, j: usize) -> T
    {
        if i < self.n && j < self.m
        {
            return self.v[j + i*self.m];
        }
        else
        {
            panic!("rustnum error : attempt to access to out of bound element in Dense matrix.");
        }
    }

    pub fn fast_get(& self, i: usize, j: usize) -> T
    {
        return self.v[j + i*self.m];
    }
/*
    pub fn block_decomposition(&self, given_from_rows: usize, given_to_rows: usize,
                               given_from_columns: usize, given_to_columns: usize) -> Dense
    {
        /*
        Extract the block between given indexes
        */

        let to_return = Dense {
            n: given_to_rows - given_from_rows,
            m: given_to_columns - given_from_columns,
            sub_matrix: true,
            values_n: self.n,
            from_rows: given_from_rows + self.from_rows,
            from_columns: given_from_columns + self.from_columns,
            to_rows: given_to_rows,
            to_columns: given_to_columns,
            v: Rc::clone(&self.v)
        };

        return to_return;
    }

    pub fn is_sub_matrix(&self) -> bool 
	{
		return self.sub_matrix;
	}
*/
}
